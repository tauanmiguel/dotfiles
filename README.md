dotfiles
========

#### Requirements

* [Neovim](https://github.com/neovim/neovim)
* [Nerd Fonts](https://aur.archlinux.org/packages/nerd-fonts-complete)
* [Prezto](https://github.com/sorin-ionescu/prezto)
* [Stow](https://www.archlinux.org/packages/community/any/stow/)
* [Tmux](https://github.com/tmux/tmux)
* [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm)
