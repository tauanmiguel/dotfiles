docker-kill-all() {
    docker kill $(docker ps -a -q);
}

docker-stop-all() {
    docker stop $(docker ps -a -q);
}

docker-rm-all() {
    docker rm $(docker ps -a -q);
}

docker-rmi-all() {
    docker rmi $(docker images -a -q);
}

alias dc-restart="docker-compose stop && docker-compose start"

alias dc-downup="docker-compose down && docker-compose up -d"

alias dc="docker-compose"
