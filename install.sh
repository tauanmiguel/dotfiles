#!/usr/bin/env bash

cd ~/.dotfiles && \
    stow git && \
    stow tmux && \
    cd ~/.dotfiles/zsh && \
    stow configs -t ~/ && \
    ln -s ~/.dotfiles/tmux/.tmux.conf ~/.tmux.conf
